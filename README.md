# README #

This is a small project containing data mined from Project Gutenberg www.gutenberg.org and GeoNames.org stored in CSV files.

The CSV files are intended to be used with a Neo4j tutorial to create an instance of a graph database for teaching and learning purposes.